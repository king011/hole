module gitlab.com/king011/hole

go 1.12

require (
	github.com/google/go-jsonnet v0.15.0
	github.com/spf13/cobra v0.0.6
	gitlab.com/king011/king-go v0.0.10
	go.uber.org/zap v1.14.0
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
)
