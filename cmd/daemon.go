package cmd

import (
	"log"

	"gitlab.com/king011/hole/cmd/daemon"
	"gitlab.com/king011/hole/configure"
	"gitlab.com/king011/hole/logger"
	"gitlab.com/king011/hole/utils"

	"github.com/spf13/cobra"
)

func init() {
	var opts daemon.Options
	var filename string
	basePath := utils.BasePath()
	cmd := &cobra.Command{
		Use:   "daemon",
		Short: "run as daemon",
		Run: func(cmd *cobra.Command, args []string) {
			// load configure
			cnf := configure.Single()
			e := cnf.Load(filename)
			if e != nil {
				log.Fatalln(e)
			}
			e = cnf.Format(basePath)
			if e != nil {
				log.Fatalln(e)
			}

			// init logger
			e = logger.Init(basePath, &cnf.Logger)
			if e != nil {
				log.Fatalln(e)
			}

			// run
			daemon.Run(opts)
		},
	}
	flags := cmd.Flags()
	flags.StringVar(&filename, "config",
		utils.Abs(basePath, "hole.jsonnet"),
		"configure file",
	)
	flags.BoolVarP(&opts.Debug, "debug",
		"d",
		false,
		"run as debug",
	)
	flags.BoolVarP(&opts.Tunnel, "tunnel",
		"t",
		false,
		"check start tunnel service",
	)
	flags.BoolVarP(&opts.Client, "client",
		"c",
		false,
		"check start tunnel service",
	)
	flags.BoolVarP(&opts.Forward, "forward",
		"f",
		false,
		"check start forward service",
	)

	rootCmd.AddCommand(cmd)
}
