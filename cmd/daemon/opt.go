package daemon

// Options 運行 參數
type Options struct {
	// 是否 要以 調試模式 運行
	Debug bool
	// 標記運行 Tunnel 服務
	Tunnel bool
	// 標記運行 Client 服務
	Client bool
	// 標記運行 Forward 服務
	Forward bool
}
