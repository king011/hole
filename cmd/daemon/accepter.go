package daemon

import (
	"crypto/tls"
	"errors"
	"io"
	"net"
	"os"
	"sync/atomic"

	"gitlab.com/king011/hole/logger"
	"gitlab.com/king011/hole/tunnel"
	"go.uber.org/zap"
	"golang.org/x/net/websocket"
)

var errAcceptAlreadyClosed = errors.New("accept already closed")

type accepterMerge struct {
	accepters []tunnel.Accepter
	cancel    chan struct{}
	closed    int32
	ch        chan io.ReadWriteCloser
}

func (a *accepterMerge) RunAccept() {
	count := len(a.accepters)
	for i := 0; i < count; i++ {
		go a.accept(a.accepters[i])
	}
}
func (a *accepterMerge) accept(accepter tunnel.Accepter) {
	ok := true
	for ok {
		c, e := accepter.Accept()
		if e == nil {
			select {
			case <-a.cancel:
				c.Close()
				ok = false
			case a.ch <- c:
			}
		} else {
			select {
			case <-a.cancel:
				ok = false
			default:
				if ce := logger.Logger.Check(zap.WarnLevel, "accepter error"); ce != nil {
					ce.Write(
						zap.Error(e),
					)
				}
			}
		}
	}
}

// Accept 接受來源連接
func (a *accepterMerge) Accept() (src io.ReadWriteCloser, e error) {
	select {
	case <-a.cancel:
		e = errAcceptAlreadyClosed
	case src = <-a.ch:
	}
	return
}

// Close 停止服務
func (a *accepterMerge) Close() (e error) {
	if atomic.CompareAndSwapInt32(&a.closed, 0, 1) {
		close(a.cancel)
		count := len(a.accepters)
		for i := 0; i < count; i++ {
			a.accepters[i].Close()
		}
	} else {
		e = errAcceptAlreadyClosed
	}
	return
}

type accepterClient struct {
	*websocket.Conn
	done   chan struct{}
	closed int32
}

func (c *accepterClient) Close() (e error) {
	e = c.Conn.Close()
	if atomic.CompareAndSwapInt32(&c.closed, 0, 1) {
		close(c.done)
	}
	return
}

func newTCPAccepter(address, certFile, keyFile string) (accepter tunnel.Accepter, safe bool, e error) {
	if certFile == "" && keyFile == "" {
		accepter, e = tunnel.NewTCPAccepter(address)
		return
	}
	safe = true
	cert, e := tls.LoadX509KeyPair(certFile, keyFile)
	if e != nil {
		return
	}
	l, e := tls.Listen("tcp", address, &tls.Config{
		Certificates: []tls.Certificate{cert},
	})
	if e != nil {
		return
	}
	accepter = tunnel.NewAccepter(l)
	return
}
func newWebsocketAccepter(address, certFile, keyFile string, route ...string) (accepter tunnel.Accepter, safe bool, e error) {
	if len(route) == 0 {
		e = errors.New("route not support nil")
		return
	}
	var l net.Listener
	var w *webscoketAccepter
	if certFile == "" && keyFile == "" {
		w = wsHelper.getWS(address)
		if w.l == nil {
			l, e = net.Listen("tcp", address)
			if e != nil {
				return
			}
			w.l = l
		}
	} else {
		w = wsHelper.getWSS(address)
		if w.l == nil {
			safe = true
			cert, e0 := tls.LoadX509KeyPair(certFile, keyFile)
			e = e0
			if e != nil {
				return
			}
			l, e = tls.Listen("tcp", address, &tls.Config{
				Certificates: []tls.Certificate{cert},
			})
			if e != nil {
				return
			}
			w.l = l
		}
	}
	helper := tunnel.NewAccepterHelper()
	for _, path := range route {
		if _, ok := w.helper[path]; ok {
			if ce := logger.Logger.Check(zap.FatalLevel, "websocket repeat route"); ce != nil {
				ce.Write(
					zap.String("path", path),
				)
			}
			os.Exit(1)
		} else {
			w.helper[path] = helper
			w.mux.Handle(path, websocket.Handler(w.accepter))
		}
	}
	accepter = helper
	return
}
