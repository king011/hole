package daemon

import (
	"fmt"
	"runtime"
	"sync"
)

func runDebug(wait *sync.WaitGroup) {
	defer wait.Done()
	var cmd string
	for {
		fmt.Print("$>")
		fmt.Scan(&cmd)
		if cmd == "num" {
			fmt.Println("NumGoroutine :", runtime.NumGoroutine())
		} else {
			fmt.Print(`num	print num goroutine
`)
		}
	}
}
