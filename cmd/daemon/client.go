package daemon

import (
	"context"
	"crypto/tls"
	"io"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"gitlab.com/king011/hole/logger"
	"gitlab.com/king011/hole/tunnel"
	"gitlab.com/king011/hole/utils"
	"go.uber.org/zap"
	"golang.org/x/net/websocket"

	"gitlab.com/king011/hole/configure"
	cnf_client "gitlab.com/king011/hole/configure/client"
)

type dialer struct {
	remote                      string
	address, origin             string
	websocket, safe, skipVerify bool
	ctx                         context.Context
	cancel                      context.CancelFunc
	timeout                     time.Duration
}

func newDialer(protocol, remote, route string, safe, skipVerify bool) *dialer {
	var address, origin string
	var websocket bool
	if protocol == "tcp" {

	} else if protocol == "websocket" {
		websocket = true
		if safe {
			address = "wss://" + remote + route
			origin = "https://" + remote + route
		} else {
			address = "ws://" + remote + route
			origin = "http://" + remote + route
		}
	} else {
		if ce := logger.Logger.Check(zap.FatalLevel, "not support protocol"); ce != nil {
			ce.Write(
				zap.String("remote", remote),
				zap.String("protocol", protocol),
			)
		}
		os.Exit(1)
	}

	ctx, cancel := context.WithCancel(context.Background())
	return &dialer{
		ctx:        ctx,
		cancel:     cancel,
		remote:     remote,
		address:    address,
		origin:     origin,
		safe:       safe,
		skipVerify: skipVerify,
		websocket:  websocket,
	}
}
func (d *dialer) Dial() (c io.ReadWriteCloser, e error) {
	var nd net.Dialer
	addr := d.remote
	var ctx context.Context
	if d.timeout >= time.Second {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(d.ctx, d.timeout)
		defer cancel()
	} else {
		ctx = d.ctx
	}
	conn, e := nd.DialContext(ctx, "tcp", addr)
	if e != nil {
		return
	}
	if d.safe {
		config := &tls.Config{
			InsecureSkipVerify: d.skipVerify,
		}
		colonPos := strings.LastIndex(addr, ":")
		if colonPos == -1 {
			colonPos = len(addr)
		}
		hostname := addr[:colonPos]

		// If no ServerName is set, infer the ServerName
		// from the hostname we're connecting to.
		if config.ServerName == "" {
			// Make a copy to avoid polluting argument or default.
			c := config.Clone()
			c.ServerName = hostname
			config = c
		}
		conn = tls.Client(conn, config)
	}

	if d.websocket {
		var cnf *websocket.Config
		cnf, e = websocket.NewConfig(d.address, d.origin)
		if e != nil {
			conn.Close()
			return
		}
		var ws *websocket.Conn
		ws, e = websocket.NewClient(cnf, conn)
		if e != nil {
			conn.Close()
			return
		}
		ws.PayloadType = websocket.BinaryFrame
		c = ws
	} else {
		c = conn
	}
	return
}
func (d *dialer) Close() (e error) {
	d.cancel()
	return
}

func runClient(wait *sync.WaitGroup, ch chan bool) {
	cnf := configure.Single().Client
	var c _Client
	count := len(cnf.Server)
	for i := 0; i < count; i++ {
		srv := c.init(&cnf.Server[i])
		if srv != nil {
			wait.Add(1)
			go runServe(wait, srv)
		}
	}
	wait.Done()
	ch <- true
}

type _Client struct {
}

func (t _Client) init(cnf *cnf_client.Server) (srv *tunnel.Client) {
	if cnf.Target == "" || cnf.Source == "" {
		return
	}

	privateKey, e := utils.LoadPrivateKey(cnf.Option.PrivateKey)
	if e != nil {
		logger.Logger.Fatal("can't load private key",
			zap.Error(e),
		)
		os.Exit(1)
	}
	src := newDialer(cnf.Protocol, cnf.Source, cnf.Route, cnf.Safe, cnf.SkipVerify)
	dst := newDialer("tcp", cnf.Target, "", false, false)
	srv = tunnel.NewClient(
		src,
		dst,
		privateKey,
		tunnel.ClientReadyTimeout(cnf.Option.ReadyTimeout),
		tunnel.ClientTimeout(cnf.Option.Timeout),
		tunnel.ClientForwardBufferSize(cnf.Option.ForwardBufferSize),
		tunnel.ClientBacklog(cnf.Option.Backlog),
	)
	return
}
