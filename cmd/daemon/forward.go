package daemon

import (
	"io"
	"os"
	"sync"

	"gitlab.com/king011/hole/configure"
	cnf_forward "gitlab.com/king011/hole/configure/forward"
	"gitlab.com/king011/hole/logger"
	"gitlab.com/king011/hole/tunnel"
	"go.uber.org/zap"
)

func runForward(wait *sync.WaitGroup, ch chan bool) {
	cnf := configure.Single().Forward
	var f _Forward
	count := len(cnf.Server)
	for i := 0; i < count; i++ {
		srv := f.init(&cnf.Server[i])
		if srv != nil {
			wait.Add(1)
			go runServe(wait, srv)
		}
	}
	wait.Done()
	ch <- true
}

type _Forward struct {
}

func (f _Forward) init(cnf *cnf_forward.Server) (srv *tunnel.Tunnel) {
	if cnf.Target.Target == "" {
		return
	}
	accepter := f.createAccepters(cnf.Source)
	if accepter == nil {
		return
	}

	dst := newDialer(cnf.Target.Protocol, cnf.Target.Target, cnf.Target.Route, cnf.Target.Safe, cnf.Target.SkipVerify)
	dst.timeout = cnf.Option.DialTimeout

	srv = tunnel.New(accepter,
		dst,
		tunnel.Timeout(cnf.Option.Timeout),
		tunnel.ForwardBufferSize(cnf.Option.ForwardBufferSize),
		tunnel.Backlog(cnf.Option.Backlog),
		tunnel.MaxDialInterval(cnf.Option.MaxDialInterval),
	)
	return
}
func (f _Forward) createAccepters(cnf []cnf_forward.Source) (accepter tunnel.Accepter) {
	count := len(cnf)
	if count == 0 {
		return
	}
	accepters := make([]tunnel.Accepter, 0, count)
	for i := 0; i < count; i++ {
		accepter := createAccepter("forward", cnf[i].Listen, cnf[i].Protocol, cnf[i].CertFile, cnf[i].KeyFile, cnf[i].Route...)
		if accepter != nil {
			accepters = append(accepters, accepter)
		}
	}

	count = len(accepters)
	switch count {
	case 0:
		return
	case 1:
		accepter = accepters[0]
	default:
		am := &accepterMerge{
			accepters: accepters,
			cancel:    make(chan struct{}),
			ch:        make(chan io.ReadWriteCloser),
		}
		go am.RunAccept()
		accepter = am
	}
	return
}
func createAccepter(name, address, protocol, certFile, keyFile string, route ...string) tunnel.Accepter {
	if address == "" {
		return nil
	}
	if protocol == "tcp" {
		accepter, safe, e := newTCPAccepter(address, certFile, keyFile)
		if e != nil {
			if ce := logger.Logger.Check(zap.FatalLevel, "tunnel listen error"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("name", name),
					zap.String("address", address),
					zap.String("protocol", protocol),
					zap.Bool("tls", safe),
				)
			}
			os.Exit(1)
		}

		if ce := logger.Logger.Check(zap.InfoLevel, "tunnel listen"); ce != nil {
			ce.Write(
				zap.String("name", name),
				zap.String("address", address),
				zap.String("protocol", protocol),
				zap.Bool("tls", safe),
			)
		}
		return accepter
	} else if protocol == "websocket" {
		accepter, safe, e := newWebsocketAccepter(address, certFile, keyFile, route...)
		if e != nil {
			if ce := logger.Logger.Check(zap.FatalLevel, "tunnel listen error"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.String("name", name),
					zap.String("address", address),
					zap.String("protocol", protocol),
					zap.Bool("tls", safe),
					zap.Strings("route", route),
				)
			}
			os.Exit(1)
		}

		if ce := logger.Logger.Check(zap.InfoLevel, "tunnel listen"); ce != nil {
			ce.Write(
				zap.String("name", name),
				zap.String("address", address),
				zap.String("protocol", protocol),
				zap.Bool("tls", safe),
				zap.Strings("route", route),
			)
		}
		return accepter
	} else {
		if ce := logger.Logger.Check(zap.FatalLevel, "not support protocol"); ce != nil {
			ce.Write(
				zap.String("name", name),
				zap.String("address", address),
				zap.String("protocol", protocol),
			)
		}
		os.Exit(1)
	}
	return nil
}
