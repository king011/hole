package daemon

import (
	"io"
	"os"
	"sync"

	"gitlab.com/king011/hole/configure"
	cnf_tunnel "gitlab.com/king011/hole/configure/tunnel"
	"gitlab.com/king011/hole/logger"
	"gitlab.com/king011/hole/tunnel"
	"gitlab.com/king011/hole/utils"
	"go.uber.org/zap"
)

type serve interface {
	Serve()
}

func runServe(wait *sync.WaitGroup, srv serve) {
	srv.Serve()
	wait.Done()
}
func runTunnel(wait *sync.WaitGroup, ch chan bool) {
	cnf := configure.Single().Tunnel
	var t _Tunnel
	count := len(cnf.Server)
	for i := 0; i < count; i++ {
		srv := t.init(&cnf.Server[i])
		if srv != nil {
			wait.Add(1)
			go runServe(wait, srv)
		}
	}
	wait.Done()
	ch <- true
}

type _Tunnel struct {
}

func (t _Tunnel) init(cnf *cnf_tunnel.Server) (srv *tunnel.Tunnel) {
	publicAccepter := t.createAccepters("public", cnf.PublicAccepter)
	if publicAccepter == nil {
		return
	}
	privateAccepter := t.createAccepters("private", cnf.PrivateAccepter)
	if privateAccepter == nil {
		publicAccepter.Close()
		return
	}
	publicKey, e := utils.LoadPublicKey(cnf.Option.PublicKey)
	if e != nil {
		logger.Logger.Fatal("can't load public key",
			zap.Error(e),
		)
		os.Exit(1)
	}

	dialer := tunnel.NewDialer(
		privateAccepter,
		publicKey,
		tunnel.DialerDialTimeout(cnf.Option.DialTimeout),
		tunnel.DialerMessageTimeout(cnf.Option.MessageTimeout),
		tunnel.DialerBacklog(cnf.Option.DialerBacklog),
		tunnel.DialerReadyCheck(cnf.Option.ReadyCheck),
	)
	srv = tunnel.New(publicAccepter,
		dialer,
		tunnel.Timeout(cnf.Option.Timeout),
		tunnel.ForwardBufferSize(cnf.Option.ForwardBufferSize),
		tunnel.Backlog(cnf.Option.Backlog),
		tunnel.MaxDialInterval(cnf.Option.MaxDialInterval),
	)
	return
}

func (t _Tunnel) createAccepters(name string, opts []cnf_tunnel.Accepter) (accepter tunnel.Accepter) {
	count := len(opts)
	if count == 0 {
		return
	}
	accepters := make([]tunnel.Accepter, 0, count)
	for i := 0; i < count; i++ {
		accepter := t.createAccepter(name, &opts[i])
		if accepter != nil {
			accepters = append(accepters, accepter)
		}
	}

	count = len(accepters)
	switch count {
	case 0:
		return
	case 1:
		accepter = accepters[0]
	default:
		am := &accepterMerge{
			accepters: accepters,
			cancel:    make(chan struct{}),
			ch:        make(chan io.ReadWriteCloser),
		}
		go am.RunAccept()
		accepter = am
	}
	return
}

func (t _Tunnel) createAccepter(name string, opt *cnf_tunnel.Accepter) tunnel.Accepter {
	return createAccepter(name, opt.Listen, opt.Protocol, opt.CertFile, opt.KeyFile, opt.Route...)
}
