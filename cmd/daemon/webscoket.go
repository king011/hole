package daemon

import (
	"net"
	"net/http"

	"gitlab.com/king011/hole/logger"
	"gitlab.com/king011/hole/tunnel"
	"go.uber.org/zap"
	"golang.org/x/net/websocket"
)

var wsHelper webscoketHelper

type webscoketHelper struct {
	ws  map[string]*webscoketAccepter
	wss map[string]*webscoketAccepter
}

func (w *webscoketHelper) run() {
	if w.ws != nil {
		for _, ws := range w.ws {
			go ws.Serve()
		}
	}
	if w.ws != nil {
		for _, wss := range w.wss {
			go wss.Serve()
		}
	}
}
func (w *webscoketHelper) getWS(address string) (val *webscoketAccepter) {
	if w.ws == nil {
		w.ws = make(map[string]*webscoketAccepter)
	} else {
		val = w.ws[address]
	}
	if val == nil {
		val = &webscoketAccepter{
			helper: make(map[string]*tunnel.AccepterHelper),
			mux:    http.NewServeMux(),
		}
		w.ws[address] = val
	}
	return
}
func (w *webscoketHelper) getWSS(address string) (val *webscoketAccepter) {
	if w.wss == nil {
		w.wss = make(map[string]*webscoketAccepter)
	} else {
		val = w.wss[address]
	}
	if val == nil {
		val = &webscoketAccepter{
			helper: make(map[string]*tunnel.AccepterHelper),
			mux:    http.NewServeMux(),
		}
		w.wss[address] = val
	}
	return
}

type webscoketAccepter struct {
	l      net.Listener
	mux    *http.ServeMux
	helper map[string]*tunnel.AccepterHelper
}

func (w *webscoketAccepter) accepter(ws *websocket.Conn) {
	path := ws.Request().URL.Path
	helper := w.helper[path]
	if helper == nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "webscoketAccepter not found helper"); ce != nil {
			ce.Write(
				zap.String("path", path),
			)
		}
		ws.Close()
		return
	}
	ws.PayloadType = websocket.BinaryFrame
	c := &accepterClient{
		Conn: ws,
		done: make(chan struct{}),
	}

	e := helper.Put(c)
	if e == nil {
		logger.Logger.Debug("websocket accepter put success")
		<-c.done
	} else {
		logger.Logger.Debug("websocket accepter put error",
			zap.Error(e),
		)
		ws.Close()
	}
}
func (w *webscoketAccepter) Serve() {
	http.Serve(w.l, w.mux)
}
