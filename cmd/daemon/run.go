package daemon

import (
	"sync"
)

// Run 運行 服務
func Run(opts Options) {
	var wait sync.WaitGroup
	n := 0
	ch := make(chan bool)
	if opts.Tunnel {
		n++
		wait.Add(1)
		go runTunnel(&wait, ch)
		<-ch
	}
	if opts.Client {
		n++
		wait.Add(1)
		go runClient(&wait, ch)
		<-ch
	}
	if opts.Forward {
		n++
		wait.Add(1)
		go runForward(&wait, ch)
		<-ch
	}

	if n == 0 {
		n = 1
		wait.Add(3)
		go runTunnel(&wait, ch)
		<-ch
		go runClient(&wait, ch)
		<-ch
		go runForward(&wait, ch)
		<-ch
	}

	if opts.Debug {
		wait.Add(1)
		go runDebug(&wait)
	}
	wsHelper.run()
	wait.Wait()
}
