package cmd

import (
	"log"

	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"

	"github.com/spf13/cobra"
	"gitlab.com/king011/hole/utils"
)

func init() {
	var publicKey, privateKey string
	var bitSize int
	basePath := utils.BasePath()
	cmd := &cobra.Command{
		Use:   "rsa",
		Short: "generate rsa key",
		Run: func(cmd *cobra.Command, args []string) {
			reader := rand.Reader
			key, e := rsa.GenerateKey(reader, bitSize)
			if e != nil {
				log.Fatalln(e)
			}
			// test key
			_, e = rsa.EncryptOAEP(sha256.New(), reader, &key.PublicKey, []byte("cebreus is an idea"), nil)
			if e != nil {
				log.Fatalln(e)
			}

			e = utils.SavePublicKey(publicKey, &key.PublicKey)
			if e != nil {
				log.Fatalln(e)
			}
			e = utils.SavePrivateKey(privateKey, key)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVar(&publicKey, "pub",
		utils.Abs(basePath, "rsa.pub"),
		"output public key path",
	)
	flags.StringVar(&privateKey, "pri",
		utils.Abs(basePath, "rsa.pri"),
		"output private key path",
	)
	flags.IntVarP(&bitSize, "bits",
		"b",
		2048,
		"key bit size",
	)
	rootCmd.AddCommand(cmd)
}
