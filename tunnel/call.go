package tunnel

import (
	"context"
	"errors"
	"io"
	"time"

	"gitlab.com/king011/hole/tunnel/message"
)

// AsyncCallResult 異步讀寫 返回值
type AsyncCallResult struct {
	Err   error
	Count int
}

// AsyncCall 發送 request 並接收 response
//
// 如果發送錯誤 AsyncCall 會自動 關閉timer 並調用 c.Close
//
// 如果 error 不爲 context.DeadlineExceeded 則定時器 海沒有完成
func AsyncCall(c io.ReadWriteCloser, request []byte, response []byte, timeout *time.Timer, done chan AsyncCallResult, cancel <-chan struct{}) (requestCount, responseCount int, e error) {
	if len(request) > 0 {
		go AsyncRequest(c, request, done)
		select {
		case <-cancel:
			c.Close()
			result := <-done
			requestCount = result.Count
			e = context.Canceled
			return
		case <-timeout.C:
			c.Close()
			result := <-done
			requestCount = result.Count
			e = context.DeadlineExceeded
			return
		case result := <-done:
			requestCount = result.Count
			e = result.Err
			if e != nil {
				c.Close()
				return
			}
		}
	}
	if len(response) > message.HeaderLength {
		go AsyncResponse(c, response, done)
		select {
		case <-cancel:
			c.Close()
			result := <-done
			requestCount = result.Count
			e = context.Canceled
			return
		case <-timeout.C:
			c.Close()
			result := <-done
			responseCount = result.Count
			e = context.DeadlineExceeded
			return
		case result := <-done:
			responseCount = result.Count
			e = result.Err
			if e != nil {
				c.Close()
				return
			}
		}
	}
	return
}

// AsyncResponse 從 reader 讀取 數據 並通過 done 返回讀取結果
func AsyncResponse(reader io.Reader, response []byte, done chan<- AsyncCallResult) {
	var result AsyncCallResult
	n, e := io.ReadFull(reader, response[:message.HeaderLength])
	result.Count = n
	if e != nil {
		result.Err = e
		done <- result
		return
	}
	count := message.GetLen(response)
	if len(response) < int(count) {
		result.Count = n
		result.Err = errors.New("out of response buffer")
		done <- result
		return
	}

	n, e = io.ReadFull(reader, response[message.HeaderLength:count])
	result.Count += n
	result.Err = e
	done <- result
}

// AsyncRequest 向 writer 寫入 數據 並通過 done 返回寫入結果
func AsyncRequest(writer io.Writer, request []byte, done chan<- AsyncCallResult) {
	var result AsyncCallResult
	result.Count, result.Err = writer.Write(request)
	done <- result
}

// SyncResponse 從 reader 讀取 數據
func SyncResponse(reader io.Reader, response []byte) (n int, e error) {
	n, e = io.ReadFull(reader, response[:message.HeaderLength])
	if e != nil {
		return
	}
	count := message.GetLen(response)
	if len(response) < int(count) {
		e = errors.New("out of response buffer")
		return
	}

	bodyCount, e := io.ReadFull(reader, response[message.HeaderLength:count])
	n += bodyCount
	return
}
