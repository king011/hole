package tunnel

import (
	"io"
	"sync/atomic"
	"time"

	"gitlab.com/king011/hole/logger"
	"go.uber.org/zap"
)

// Tunnel 隧道
type Tunnel struct {
	opts options

	accepter Accepter
	dialer   Dialer
	canceled int32

	cancel  chan struct{}
	backlog chan io.ReadWriteCloser
}

// New 創建一個 隧道
func New(accepter Accepter, dialer Dialer, options ...Option) *Tunnel {
	if accepter == nil {
		panic("new tunnel from nil accepter")
	}
	if dialer == nil {
		panic("new tunnel from nil dialer")
	}

	opts := defaultOptions
	for _, option := range options {
		option.apply(&opts)
	}
	return &Tunnel{
		opts:     opts,
		accepter: accepter,
		dialer:   dialer,

		cancel:  make(chan struct{}),
		backlog: make(chan io.ReadWriteCloser),
	}
}

// Stop 停止轉發服務 已經開始的 轉發任務會繼續運行
func (t *Tunnel) Stop() {
	if atomic.CompareAndSwapInt32(&t.canceled, 0, 1) {
		t.accepter.Close()
		t.dialer.Close()
		close(t.cancel)
	}
}

// Serve 執行服務
func (t *Tunnel) Serve() {
	if ce := logger.Logger.Check(zap.DebugLevel, "Tunnel Serve Start"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "Tunnel Serve Stop"); ce != nil {
				ce.Write()
			}
		}()
	}
	for i := 0; i < t.opts.backlog; i++ {
		go t.serveBacklog()
	}

	accepter := t.accepter
	for {
		src, e := accepter.Accept()
		if e == nil {
			select {
			case <-t.cancel:
				src.Close()
				return
			case t.backlog <- src:
			}
		} else {
			if ce := logger.Logger.Check(zap.DebugLevel, "Tunnel Accept"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			if atomic.LoadInt32(&t.canceled) == 0 {
				continue
			} else {
				break
			}
		}
	}
	return
}
func (t *Tunnel) serveBacklog() {
	maxDialInterval := t.opts.maxDialInterval
	delay := time.Second
	var ok bool
	var timer *time.Timer
	for {
		select {
		case <-t.cancel:
			return
		case src := <-t.backlog:
			ok = t.dial(src)
			if ok {
				delay = time.Second
			} else {
				if ce := logger.Logger.Check(zap.WarnLevel, "tunnel dial delay"); ce != nil {
					ce.Write(
						zap.Duration("duration", delay),
					)
				}
				if timer == nil {
					timer = time.NewTimer(delay)
				} else {
					timer.Reset(delay)
				}
				delay *= 2
				if delay > maxDialInterval {
					delay = maxDialInterval
				}
				select {
				case <-t.cancel:
					if !timer.Stop() {
						<-timer.C
					}
					return
				case <-timer.C:
				}
			}
		}
	}
}
func (t *Tunnel) dial(src io.ReadWriteCloser) (ok bool) {
	if ce := logger.Logger.Check(zap.DebugLevel, "Tunnel dial Start"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "Tunnel dial Stop"); ce != nil {
				ce.Write()
			}
		}()
	}
	dst, e := t.dialer.Dial()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Tunnel dial error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		src.Close()
		return
	}

	if ce := logger.Logger.Check(zap.DebugLevel, "Tunnel dial success"); ce != nil {
		ce.Write(
			zap.Error(e),
		)
	}
	ok = true

	bufferSize := t.opts.forwardBufferSize
	srcBuffer := make([]byte, bufferSize)
	dstBuffer := make([]byte, bufferSize)

	var f forward
	go f.serve(src, dst, srcBuffer, dstBuffer, t.opts.timeout)
	return
}
