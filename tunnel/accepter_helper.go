package tunnel

import (
	"errors"
	"io"
	"sync/atomic"
)

// AccepterHelper 一個實現 Accepter 接口的 輔助工具
type AccepterHelper struct {
	canceled int32
	done     chan struct{}
	ready    chan io.ReadWriteCloser
}

// NewAccepterHelper 創建 AccepterHelper
func NewAccepterHelper() *AccepterHelper {
	return &AccepterHelper{
		done:  make(chan struct{}),
		ready: make(chan io.ReadWriteCloser),
	}
}

// Accept 接受來源連接
func (accepter *AccepterHelper) Accept() (src io.ReadWriteCloser, e error) {
	select {
	case c := <-accepter.ready:
		src = c
	case <-accepter.done:
		e = errors.New("accepter already closed")
	}
	return
}

// Close 停止服務
func (accepter *AccepterHelper) Close() (e error) {
	if atomic.CompareAndSwapInt32(&accepter.canceled, 0, 1) {
		close(accepter.done)
	}
	return
}

// Put 壓入一個就緒 的 數據管道
func (accepter *AccepterHelper) Put(c io.ReadWriteCloser) (e error) {
	if c == nil {
		e = errors.New("not support put nil")
		return
	}
	select {
	case <-accepter.done:
		e = errors.New("accepter already closed")
	case accepter.ready <- c:
	}
	return
}

// Done 服務停止 信號
func (accepter *AccepterHelper) Done() chan<- struct{} {
	return accepter.done
}
