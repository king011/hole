package tunnel

import "io"

// Accepter 接收數據源
type Accepter interface {
	// Accept 接受來源連接
	Accept() (src io.ReadWriteCloser, e error)
	// Close 停止服務
	Close() error
}

// Dialer 撥號器
type Dialer interface {
	// Dial 向目標 撥號
	Dial() (dst io.ReadWriteCloser, e error)
	// Close 停止服務
	Close() error
}
