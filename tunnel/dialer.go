package tunnel

import (
	"bytes"
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"io"
	"sync/atomic"
	"time"

	"gitlab.com/king011/hole/logger"
	"gitlab.com/king011/hole/tunnel/message"
	"go.uber.org/zap"
)

type dialerListener struct {
	opts dialerOptions

	accepter  Accepter
	publicKey *rsa.PublicKey

	canceled int32
	cancel   chan struct{}
	ready    chan *dialerClient
	backlog  chan io.ReadWriteCloser
}

// NewDialer 創建轉發 撥號器
func NewDialer(accepter Accepter, publicKey *rsa.PublicKey, options ...DialerOption) Dialer {
	if accepter == nil {
		panic("new dialer from nil accepter")
	}
	if publicKey == nil {
		panic("new dialer from nil public key")
	}
	opts := defaultDialerOptions
	for _, option := range options {
		option.apply(&opts)
	}

	dialer := &dialerListener{
		opts:      opts,
		accepter:  accepter,
		publicKey: publicKey,

		cancel:  make(chan struct{}),
		ready:   make(chan *dialerClient),
		backlog: make(chan io.ReadWriteCloser, 1),
	}
	go dialer.serve()
	return dialer
}

// Dial 向目標 撥號
func (dialer *dialerListener) Dial() (dst io.ReadWriteCloser, e error) {
	timer := time.NewTimer(dialer.opts.dialTimeout)
	select {
	case <-timer.C:
		e = context.DeadlineExceeded
	case <-dialer.cancel:
		if !timer.Stop() {
			<-timer.C
		}
		e = context.Canceled
	case ready := <-dialer.ready:
		atomic.StoreInt32(&ready.stop, 1)
		// 通知客戶端 撥號
		dst, e = dialer.dial(ready, timer)
		if e != context.DeadlineExceeded {
			if !timer.Stop() {
				<-timer.C
			}
		}
		if e == nil {
			if ce := logger.Logger.Check(zap.DebugLevel, "dialer Dial success"); ce != nil {
				ce.Write()
			}
		}
	}

	if e != nil {
		if ce := logger.Logger.Check(zap.DebugLevel, "dialer Dial error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	}
	return
}
func (dialer *dialerListener) dial(ready *dialerClient, timer *time.Timer) (dst io.ReadWriteCloser, e error) {
	message.SetCommand(ready.buffer, message.NetDial)
	message.SetLen(ready.buffer, message.HeaderLength)
	if ready.done == nil {
		ready.done = make(chan AsyncCallResult)
	}
	// 發送 keeplive
	// _, _, e = AsyncCall(ready.c, ready.buffer[:message.HeaderLength], ready.buffer, timer, ready.done, dialer.cancel)
	response, e := ready.asyncCall(ready.buffer[:message.HeaderLength], timer, dialer.cancel)
	// 關閉定時器
	if e != nil {
		return
	}
	if message.GetCommand(response) != message.NetDial {
		// 錯誤回覆 關閉
		ready.c.Close()
		return
	}
	dst = ready.c
	return
}
func (dialer *dialerListener) Close() (e error) {
	if atomic.CompareAndSwapInt32(&dialer.canceled, 0, 1) {
		e = dialer.accepter.Close()
		close(dialer.cancel)
	}
	return
}
func (dialer *dialerListener) isClosed() (yes bool) {
	select {
	case <-dialer.cancel:
		yes = true
	default:
	}
	return
}
func (dialer *dialerListener) serve() {
	for i := 0; i < dialer.opts.backlog; i++ {
		go dialer.serveBacklog()
	}

	for {
		c, e := dialer.accepter.Accept()
		if e != nil {
			if dialer.isClosed() {
				break
			} else {
				time.Sleep(time.Second)
				continue
			}
		}
		if !dialer.putBacklog(c) {
			c.Close()
		}
	}
	// 釋放所有 積壓的 socket
	close(dialer.backlog)
	for c := range dialer.backlog {
		c.Close()
	}
}
func (dialer *dialerListener) putBacklog(c io.ReadWriteCloser) (yes bool) {
	if ce := logger.Logger.Check(zap.DebugLevel, "dialer putBacklog Start"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "dialer putBacklog Stop"); ce != nil {
				ce.Write()
			}
		}()
	}
	// 嘗試加入隊列
	select {
	case dialer.backlog <- c:
		yes = true
		return
	case <-dialer.cancel:
		return
	default:
	}

	// 隊列已滿 剔除一個之前的 連接
	select {
	case dialer.backlog <- c:
		yes = true
	case expired := <-dialer.backlog:
		expired.Close()
		dialer.backlog <- c
		yes = true
		return
	case <-dialer.cancel:
	}
	return
}

func (dialer *dialerListener) serveBacklog() {
	for {
		select {
		case <-dialer.cancel:
			return
		case c, ok := <-dialer.backlog:
			if !ok {
				return
			}
			// 驗證 合法性
			dialer.verifyBacklog(c)
		}
	}
}
func (dialer *dialerListener) verifyBacklog(c io.ReadWriteCloser) {
	if ce := logger.Logger.Check(zap.DebugLevel, "dialer verifyBacklog In"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "dialer verifyBacklog Out"); ce != nil {
				ce.Write()
			}
		}()
	}
	// 創建 緩衝區
	msg := make([]byte, 1024)
	// 驗證合法性
	done := dialer.verify(c, msg)
	if done == nil {
		return
	}

	// 傳遞 ready 通道給消費者
	dialer.loopPutReady(c, msg)
}

func (dialer *dialerListener) verify(c io.ReadWriteCloser, msg []byte) chan AsyncCallResult {
	// 生成 隨機 驗證數據
	hashed, body, e := dialer.generateVerify(msg[:128])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "dialer generateVerify error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		c.Close()
		return nil
	}
	count := message.HeaderLength + len(body)
	copy(msg[message.HeaderLength:], body)
	message.SetCommand(msg, message.NetVerify)
	message.SetLen(msg, uint16(count))
	request := msg[:count]
	timer := time.NewTimer(dialer.opts.messageTimeout)
	done := make(chan AsyncCallResult)
	// 發送 驗證包
	_, n, e := AsyncCall(c, request, msg, timer, done, dialer.cancel)
	// 關閉定時器
	if e != nil {
		if !timer.Stop() {
			<-timer.C
		}
		if ce := logger.Logger.Check(zap.WarnLevel, "dialer NetVerify error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}

		c.Close()
		return nil
	}
	// 驗證 返回指令
	response := msg[:n]
	cmd := message.GetCommand(response)
	if cmd != message.NetVerify {
		if ce := logger.Logger.Check(zap.WarnLevel, "dialer NetVerify error"); ce != nil {
			ce.Write(
				zap.String("error", "command not match"),
				zap.Uint8("cmd", cmd),
			)
		}
		c.Close()
		return nil
	}
	// 驗證 解密
	body = response[message.HeaderLength:]
	if len(body) < len(hashed) || !bytes.Equal(body[:len(hashed)], hashed) {
		if ce := logger.Logger.Check(zap.WarnLevel, "dialer NetVerify error"); ce != nil {
			ce.Write(
				zap.String("error", "response body unknow"),
			)
		}
		c.Close()
		return nil
	}
	// 驗證簽名
	signature := body[len(hashed):]
	if len(signature) == 0 {
		if ce := logger.Logger.Check(zap.WarnLevel, "dialer NetVerify error"); ce != nil {
			ce.Write(
				zap.String("error", "response not signature"),
			)
		}
		c.Close()
		return nil
	}
	e = rsa.VerifyPKCS1v15(dialer.publicKey, crypto.SHA256, hashed[:], signature)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "dialer NetVerify error"); ce != nil {
			ce.Write(
				zap.String("error", "response signature unknow"),
			)
		}
		c.Close()
		return nil
	}
	return done
}
func (dialer *dialerListener) generateVerify(b []byte) (hashed, data []byte, e error) {
	_, e = rand.Read(b)
	if e != nil {
		return
	}
	tmp := sha256.Sum256(b)
	hashed = tmp[:]

	// 加密 hash
	data, e = rsa.EncryptOAEP(sha256.New(), rand.Reader, dialer.publicKey, hashed, nil)
	if e != nil {
		hashed = nil
		return
	}
	return
}
func (dialer *dialerListener) loopPutReady(c io.ReadWriteCloser, msg []byte) {
	if ce := logger.Logger.Check(zap.DebugLevel, "dialer loopPutReady In"); ce != nil {
		ce.Write()
	}

	ready := &dialerClient{
		buffer:   msg,
		c:        c,
		cancel:   make(chan struct{}),
		response: make(chan []byte, 1),
	}
	go ready.run()
	for {
		if dialer.putReady(ready) {
			break
		}
		// 檢查 存活
		if ready.done == nil {
			ready.done = make(chan AsyncCallResult)
			message.SetCommand(msg, message.NetKeeplive)
			message.SetLen(msg, message.HeaderLength)
		}
		timer := time.NewTimer(dialer.opts.messageTimeout)
		if ce := logger.Logger.Check(zap.DebugLevel, "request keeplive"); ce != nil {
			ce.Write()
		}
		// 發送 keeplive
		//	_, _, e := AsyncCall(c, msg[:message.HeaderLength], msg, timer, ready.done, dialer.cancel)
		response, e := ready.asyncCall(msg[:message.HeaderLength], timer, dialer.cancel)
		// 關閉定時器
		if e != context.DeadlineExceeded {
			if !timer.Stop() {
				<-timer.C
			}
		}
		// 驗證錯誤
		if e != nil {
			if ce := logger.Logger.Check(zap.DebugLevel, "response keeplive"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			break
		}
		if message.GetCommand(response) != message.NetKeeplive {
			// 錯誤回覆 關閉
			c.Close()
			break
		}
	}
}
func (dialer *dialerListener) putReady(ready *dialerClient) (exit bool) {
	timer := time.NewTimer(dialer.opts.readyCheck)
	select {
	case <-dialer.cancel:
		if !timer.Stop() {
			<-timer.C
		}
		ready.c.Close()
		exit = true
	case <-ready.cancel:
		if !timer.Stop() {
			<-timer.C
		}
		ready.c.Close()
		exit = true
	case dialer.ready <- ready:
		if !timer.Stop() {
			<-timer.C
		}
		exit = true
	case <-timer.C:
	}
	return
}
