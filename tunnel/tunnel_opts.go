package tunnel

import "time"

const (
	defaultTimeout           = time.Hour
	defaultForwardBufferSize = 1024 * 32
	defaultBacklog           = 5
	defaultMaxDialInterval   = time.Second * 64
)

type options struct {
	timeout           time.Duration
	forwardBufferSize int
	backlog           int
	maxDialInterval   time.Duration
}

var defaultOptions = options{
	timeout:           defaultTimeout,
	forwardBufferSize: defaultForwardBufferSize,
	backlog:           defaultBacklog,
	maxDialInterval:   defaultMaxDialInterval,
}

// Option 隧道 選項
type Option interface {
	apply(*options)
}
type funcOption struct {
	f func(*options)
}

func (fdo *funcOption) apply(do *options) {
	fdo.f(do)
}
func newFuncOption(f func(*options)) *funcOption {
	return &funcOption{
		f: f,
	}
}

// Timeout 通道內多久沒有數據流動 則關閉
func Timeout(duration time.Duration) Option {
	return newFuncOption(func(o *options) {
		if duration < time.Second {
			return
		}
		o.timeout = duration
	})
}

// ForwardBufferSize forward 緩衝區大小
func ForwardBufferSize(bufferSize int) Option {
	return newFuncOption(func(o *options) {
		if bufferSize < 1024 {
			return
		}
		o.forwardBufferSize = bufferSize
	})
}

// Backlog Accept 積壓的 socket 上限
func Backlog(backlog int) Option {
	return newFuncOption(func(o *options) {
		if backlog < 1 {
			return
		}
		o.backlog = backlog
	})
}

// MaxDialInterval 撥號失敗後 等待下次重新撥號的 最大延遲
func MaxDialInterval(duration time.Duration) Option {
	return newFuncOption(func(o *options) {
		if duration < time.Second {
			return
		}
		o.maxDialInterval = duration
	})
}
