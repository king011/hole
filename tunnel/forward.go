package tunnel

import (
	"io"
	"sync/atomic"
	"time"

	"gitlab.com/king011/hole/logger"
	"go.uber.org/zap"
)

type forward struct {
	src, dst io.ReadWriteCloser
	canceled int32
	cancel   chan struct{}
}

func (f *forward) serve(src, dst io.ReadWriteCloser, srcBuffer, dstBuffer []byte, timeout time.Duration) {
	if ce := logger.Logger.Check(zap.DebugLevel, "forward serve Start"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "forward serve Stop"); ce != nil {
				ce.Write()
			}
		}()
	}

	signal := make(chan bool)
	f.src = src
	f.dst = dst
	f.cancel = make(chan struct{})
	go f.forward(src, dst, srcBuffer, signal)
	go f.forward(dst, src, dstBuffer, signal)

	timer := time.NewTimer(timeout)
	last := time.Now()
	for {
		select {
		case <-f.cancel:
			return
		case <-signal:
			last = time.Now()
		case <-timer.C:
			// 計算 截止時間
			deadline := last.Add(timeout)
			duration := deadline.Sub(time.Now())
			if duration < time.Second {
				if ce := logger.Logger.Check(zap.DebugLevel, "forward timeout"); ce != nil {
					ce.Write()
				}
				// 超時
				f.close()
				return
			}
			// 重設定時器
			timer.Reset(duration)
		}
	}
}

func (f *forward) forward(src, dst io.ReadWriteCloser, buffer []byte, signal chan<- bool) {
	var n int
	var e error
	for {
		n, e = src.Read(buffer)
		if e != nil {
			break
		}
		_, e = dst.Write(buffer[:n])
		if e != nil {
			break
		}
		select {
		case signal <- true:
		default:
		}
	}
	f.close()
}
func (f *forward) close() {
	if atomic.CompareAndSwapInt32(&f.canceled, 0, 1) {
		f.src.Close()
		f.dst.Close()
		close(f.cancel)
	}
}
