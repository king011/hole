package tunnel

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"io"
	"math"
	"sync/atomic"
	"time"

	"gitlab.com/king011/hole/tunnel/message"

	"gitlab.com/king011/hole/logger"
	"go.uber.org/zap"
)

// Client 隧道 客戶端
type Client struct {
	opts clientOptions

	tunnel     Dialer
	dst        Dialer
	privateKey *rsa.PrivateKey

	canceled int32
	cancel   chan struct{}
	backlog  chan bool
}

// NewClient .
func NewClient(tunnel Dialer, dst Dialer, privateKey *rsa.PrivateKey, options ...ClientOption) *Client {
	if tunnel == nil {
		panic("new client from nil tunnel")
	}
	if dst == nil {
		panic("new client from nil dst")
	}
	if privateKey == nil {
		panic("new client from nil private key")
	}
	opts := defaultClientOptions
	for _, option := range options {
		option.apply(&opts)
	}
	return &Client{
		opts: opts,

		tunnel:     tunnel,
		dst:        dst,
		privateKey: privateKey,

		cancel:  make(chan struct{}),
		backlog: make(chan bool),
	}
}

// Stop 停止轉發服務 已經開始的 轉發任務會繼續運行
func (client *Client) Stop() {
	if atomic.CompareAndSwapInt32(&client.canceled, 0, 1) {
		close(client.cancel)
		client.tunnel.Close()
		client.dst.Close()
	}
}

// Serve 執行服務
func (client *Client) Serve() {
	if ce := logger.Logger.Check(zap.DebugLevel, "Client Serve Start"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "Client Serve Stop"); ce != nil {
				ce.Write()
			}
		}()
	}
	for i := 0; i < client.opts.backlog; i++ {
		go client.serveBacklog()
	}
	for {
		select {
		case <-client.cancel:
			return
		case client.backlog <- true:
		}
	}
}

func (client *Client) serveBacklog() {
	if ce := logger.Logger.Check(zap.DebugLevel, "Client serveBacklog Start"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "Client serveBacklog Stop"); ce != nil {
				ce.Write()
			}
		}()
	}

	for {
		select {
		case <-client.cancel:
			return
		case <-client.backlog:
			if !client.dialTunnel() {
				timer := time.NewTimer(time.Second)
				select {
				case <-timer.C:
				case <-client.cancel:
					if !timer.Stop() {
						<-timer.C
					}
					return
				}
			}
		}
	}
}
func (client *Client) dialTunnel() (ok bool) {
	if ce := logger.Logger.Check(zap.DebugLevel, "tunnel dial"); ce != nil {
		ce.Write()
	}
	// 向服務器撥號
	c, e := client.tunnel.Dial()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel dial  error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		timer := time.NewTimer(time.Second)
		select {
		case <-client.cancel:
			if !timer.Stop() {
				<-timer.C
			}
		case <-timer.C:
		}
		return
	}

	// 監聽服務器 信息
	msg := make([]byte, 1024)
	done := make(chan AsyncCallResult)
	var timer *time.Timer
	var exit bool
	for {
		if timer == nil {
			timer = time.NewTimer(client.opts.readyTimeout)
		} else {
			timer.Reset(client.opts.readyTimeout)
		}
		_, n, e := AsyncCall(c, nil, msg, timer, done, client.cancel)
		if e != nil {
			if e != context.DeadlineExceeded {
				if !timer.Stop() {
					<-timer.C
				}
			}
			if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel AsyncResponse  error"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		}
		exit, ok = client.onTunnelMessage(c, msg, n, done)
		if exit {
			return
		}
	}
}
func (client *Client) onTunnelMessage(c io.ReadWriteCloser, msg []byte, n int, done chan AsyncCallResult) (exit, ok bool) {
	cmd := message.GetCommand(msg)
	if ce := logger.Logger.Check(zap.DebugLevel, "tunnel message"); ce != nil {
		ce.Write(
			zap.Uint8("cmd", cmd),
		)
	}
	switch cmd {
	case message.NetKeeplive:
		exit = client.reply(c, msg[:message.HeaderLength], done)
	case message.NetVerify:
		exit = client.replyNetVerify(c, msg, n, done)
	case message.NetDial:
		ok = client.replyNetDial(c, msg, n, done)
		exit = true
	default:
		exit = true
		c.Close()
		if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel unknow command"); ce != nil {
			ce.Write(
				zap.Uint8("cmd", cmd),
			)
		}
	}
	return
}
func (client *Client) replyNetDial(c io.ReadWriteCloser, msg []byte, n int, done chan AsyncCallResult) (ok bool) {
	dst, e := client.dst.Dial()
	if e != nil {
		c.Close()
		if ce := logger.Logger.Check(zap.WarnLevel, "Client replay  NetDial error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	message.SetCommand(msg, message.NetDial)
	message.SetLen(msg, message.HeaderLength)
	exit := client.reply(c, msg[:message.HeaderLength], done)
	if exit {
		dst.Close()
		return
	}

	// 開始轉發
	bufferSize := client.opts.forwardBufferSize
	srcBuffer := make([]byte, bufferSize)
	dstBuffer := make([]byte, bufferSize)

	var f forward
	go f.serve(c, dst, srcBuffer, dstBuffer, client.opts.timeout)
	ok = true
	return
}
func (client *Client) replyNetVerify(c io.ReadWriteCloser, msg []byte, n int, done chan AsyncCallResult) (exit bool) {
	body := msg[message.HeaderLength:n]
	if len(body) == 0 {
		c.Close()
		exit = true
		if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel reply NetVerify error"); ce != nil {
			ce.Write(
				zap.String("error", "body nil"),
			)
		}
		return
	}
	// 解密
	body, e := rsa.DecryptOAEP(sha256.New(), rand.Reader, client.privateKey, body, nil)
	if e != nil {
		c.Close()
		exit = true
		if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel reply NetVerify error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	// 簽名
	signature, e := rsa.SignPKCS1v15(rand.Reader, client.privateKey, crypto.SHA256, body)
	if e != nil {
		c.Close()
		exit = true
		if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel reply NetVerify error"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 組包
	count := message.HeaderLength + len(body) + len(signature)
	if count > math.MaxUint16 {
		c.Close()
		exit = true
		if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel reply NetVerify error"); ce != nil {
			ce.Write(
				zap.String("error", "reply body to large"),
			)
		}
		return
	}
	if count > len(msg) {
		c.Close()
		exit = true
		if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel reply NetVerify error"); ce != nil {
			ce.Write(
				zap.String("error", "buffer too small"),
			)
		}
		return
	}
	message.SetCommand(msg, message.NetVerify)
	message.SetLen(msg, uint16(count))
	copy(msg[message.HeaderLength:], body)
	copy(msg[message.HeaderLength+len(body):], signature)
	exit = client.reply(c, msg[:count], done)
	return
}
func (client *Client) reply(c io.ReadWriteCloser, msg []byte, done chan AsyncCallResult) (exit bool) {
	go AsyncRequest(c, msg, done)
	select {
	case <-client.cancel:
		c.Close()
		<-done
	case result := <-done:
		if result.Err != nil {
			c.Close()
			exit = true
			if ce := logger.Logger.Check(zap.WarnLevel, "Client tunnel reply error"); ce != nil {
				ce.Write(
					zap.Error(result.Err),
				)
			}
		}
	}
	return
}
