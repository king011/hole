package tunnel

import "time"

const (
	defaultClientReadyTimeout = time.Minute * 2

	defaultClientTimeout    = time.Hour
	defaultClientBufferSize = 1024 * 32
	defaultClientBacklog    = 2
)

type clientOptions struct {
	readyTimeout time.Duration

	timeout           time.Duration
	forwardBufferSize int
	backlog           int
}

var defaultClientOptions = clientOptions{
	readyTimeout: defaultClientReadyTimeout,

	timeout:           defaultClientTimeout,
	forwardBufferSize: defaultClientBufferSize,
	backlog:           defaultClientBacklog,
}

// ClientOption 客戶端 選項
type ClientOption interface {
	apply(*clientOptions)
}
type funcClientOption struct {
	f func(*clientOptions)
}

func (fdo *funcClientOption) apply(do *clientOptions) {
	fdo.f(do)
}
func newFuncClientOption(f func(*clientOptions)) *funcClientOption {
	return &funcClientOption{
		f: f,
	}
}

// ClientTimeout 通道內多久沒有數據流動 則關閉
func ClientTimeout(duration time.Duration) ClientOption {
	return newFuncClientOption(func(o *clientOptions) {
		if duration < time.Second {
			return
		}
		o.timeout = duration
	})
}

// ClientForwardBufferSize forward 緩衝區大小
func ClientForwardBufferSize(bufferSize int) ClientOption {
	return newFuncClientOption(func(o *clientOptions) {
		if bufferSize < 1024 {
			return
		}
		o.forwardBufferSize = bufferSize
	})
}

// ClientBacklog Accept 積壓的 socket 上限
func ClientBacklog(backlog int) ClientOption {
	return newFuncClientOption(func(o *clientOptions) {
		if backlog < 1 {
			return
		}
		o.backlog = backlog
	})
}

// ClientReadyTimeout 空閒 連接 超時時間
func ClientReadyTimeout(duration time.Duration) ClientOption {
	return newFuncClientOption(func(o *clientOptions) {
		if duration < time.Second {
			return
		}
		o.readyTimeout = duration
	})
}
