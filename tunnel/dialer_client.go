package tunnel

import (
	"context"
	"errors"
	"io"
	"sync/atomic"
	"time"

	"gitlab.com/king011/hole/logger"
	"go.uber.org/zap"
)

type dialerClient struct {
	buffer []byte
	done   chan AsyncCallResult
	c      io.ReadWriteCloser

	cancel   chan struct{}
	response chan []byte

	stop int32
}

func (client *dialerClient) run() {
	if ce := logger.Logger.Check(zap.DebugLevel, "dialerClient run Start"); ce != nil {
		ce.Write()
		defer func() {
			if ce := logger.Logger.Check(zap.DebugLevel, "dialerClient run Stop"); ce != nil {
				ce.Write()
			}
		}()
	}

	b := make([]byte, 128)
	for {
		n, e := SyncResponse(client.c, b)
		if e != nil {
			close(client.cancel)
			break
		}
		if n == 0 {
			continue
		}
		response := make([]byte, n)
		copy(response, b)
		select {
		case client.response <- response:
		default:
			if ce := logger.Logger.Check(zap.WarnLevel, "unexpected response"); ce != nil {
				ce.Write()
			}
			close(client.cancel)
			return
		}
		if atomic.LoadInt32(&client.stop) != 0 {
			break
		}
	}
}
func (client *dialerClient) asyncCall(request []byte, timeout *time.Timer, cancel <-chan struct{}) (response []byte, e error) {
	go AsyncRequest(client.c, request, client.done)
	select {
	case <-client.cancel:
		client.c.Close()
		<-client.done
		e = context.Canceled
		return
	case <-cancel:
		client.c.Close()
		<-client.done
		e = context.Canceled
		return
	case <-timeout.C:
		client.c.Close()
		<-client.done
		e = context.DeadlineExceeded
		return
	case result := <-client.done:
		e = result.Err
		if e != nil {
			client.c.Close()
			return
		}
	}

	select {
	case <-client.cancel:
		client.c.Close()
		e = context.Canceled
		return
	case <-cancel:
		client.c.Close()
		e = context.Canceled
		return
	case <-timeout.C:
		client.c.Close()
		e = context.DeadlineExceeded
		return
	case response = <-client.response:
		if response == nil {
			e = errors.New("response nil")
		}
	}
	return
}
