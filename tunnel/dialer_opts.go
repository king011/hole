package tunnel

import (
	"time"
)

const (
	defaultDialerDialTimeout    = time.Second * 20
	defaultDialerMessageTimeout = time.Second * 20

	defaultDialerBacklog    = 5
	defaultDialerReadyCheck = time.Minute
)

type dialerOptions struct {
	dialTimeout    time.Duration
	messageTimeout time.Duration
	backlog        int
	readyCheck     time.Duration
}

var defaultDialerOptions = dialerOptions{
	dialTimeout:    defaultDialerDialTimeout,
	messageTimeout: defaultDialerMessageTimeout,
	backlog:        defaultDialerBacklog,
	readyCheck:     defaultDialerReadyCheck,
}

// DialerOption 撥號器 選項
type DialerOption interface {
	apply(*dialerOptions)
}
type funcDialerOption struct {
	f func(*dialerOptions)
}

func (fdo *funcDialerOption) apply(do *dialerOptions) {
	fdo.f(do)
}
func newFuncDialerOption(f func(*dialerOptions)) *funcDialerOption {
	return &funcDialerOption{
		f: f,
	}
}

// DialerDialTimeout 設置 撥號 超時
func DialerDialTimeout(duration time.Duration) DialerOption {
	return newFuncDialerOption(func(o *dialerOptions) {
		if duration < time.Second {
			return
		}
		o.dialTimeout = duration
	})
}

// DialerMessageTimeout 消息響應超時時間
func DialerMessageTimeout(duration time.Duration) DialerOption {
	return newFuncDialerOption(func(o *dialerOptions) {
		if duration < time.Second {
			return
		}
		o.messageTimeout = duration
	})
}

// DialerBacklog Accept 積壓的 socket 上限
func DialerBacklog(backlog int) DialerOption {
	return newFuncDialerOption(func(o *dialerOptions) {
		if backlog < 1 {
			return
		}
		o.backlog = backlog
	})
}

// DialerReadyCheck 已經就緒的 socket 多久檢測一次 鏈路是否正常
func DialerReadyCheck(duration time.Duration) DialerOption {
	return newFuncDialerOption(func(o *dialerOptions) {
		if duration < time.Second {
			return
		}
		o.readyCheck = duration
	})
}
