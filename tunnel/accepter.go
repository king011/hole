package tunnel

import (
	"io"
	"net"
)

type accepterListener struct {
	l net.Listener
}

// NewAccepter 創建來源接收器
func NewAccepter(l net.Listener) Accepter {
	if l == nil {
		panic("new accepter from nil")
	}
	return accepterListener{
		l: l,
	}
}

// Accept .
func (accepter accepterListener) Accept() (src io.ReadWriteCloser, e error) {
	return accepter.l.Accept()
}

// Close .
func (accepter accepterListener) Close() (e error) {
	return accepter.l.Close()
}

// NewTCPAccepter 創建 tcp Accepter
func NewTCPAccepter(address string) (accepter Accepter, e error) {
	l, e := net.Listen("tcp", address)
	if e != nil {
		return
	}
	accepter = NewAccepter(l)
	return
}
