package message

import "encoding/binary"

const (
	// HeaderLength 消息頭 固定長度
	HeaderLength = 3
	// NetNil 無效指令
	NetNil = 0
	// NetVerify 驗證合法性
	NetVerify = 1
	// NetKeeplive 心跳 防止 掉線
	NetKeeplive = 2
	// NetDial 執行 撥號
	NetDial = 3
)

// SetCommand 設置指令
func SetCommand(msg []byte, cmd uint8) {
	msg[0] = cmd
}

// GetCommand 返回指令
func GetCommand(msg []byte) uint8 {
	return msg[0]
}

// SetLen 設置 消息長度
func SetLen(msg []byte, n uint16) {
	binary.LittleEndian.PutUint16(msg[1:], n)
}

// GetLen 返回 消息長度
func GetLen(msg []byte) uint16 {
	return binary.LittleEndian.Uint16(msg[1:])
}
