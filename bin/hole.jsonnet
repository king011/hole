local Millisecond = 1;
local Second = 1000 * Millisecond;
local Minute = 60 * Second;
local Hour = 60 * Minute;
local Day = 24 * Hour;
local UseTCP = "tcp";
local UseWebsocket = "websocket";
{
	// 反向映射通道
	Tunnel: {
		// Server 全局默認配置
		Option: {
			//  客戶註冊時 安全驗證使用的 公鑰 
			// 必須是有效的 rsa public key
			PublicKey: "rsa.pub",
			// 建立好的 通道內 多長時間沒有數據 則自動斷開連接
			// 最小值是 Second
			Timeout : Hour,
			// 在 轉發通道內 單個方向的 數據時 創建 緩衝區大小
			// 通道是 全雙工的 故每個通道有 兩個 緩衝區 分別用於 讀寫
			// 最小值是 1024
			ForwardBufferSize: 1024 * 32,
			// 如果 轉發客戶端端 註冊了一個 PrivateAccepter 此時沒有 數據源連接 PublicAccepter 將積壓一個 空閒的 通道
			// Backlog 設置允許的最大積壓數
			// 達到 Backlog 上限 PrivateAccepter 將 停止 接收 新的 註冊
			// 最小值是 1
			Backlog: 5,

			// PrivateAccepter 向註冊的 轉發客戶 發送 撥號請求的 超時時間
			// 最小值是 Second
			DialTimeout: Second * 20,
			// 等待響應的 超時時間
			// 最小值是 Second
			MessageTimeout: Second * 20,
			// 同時允許的 最大 撥號數量
			// 最小值是 1
			DialerBacklog: 5,
			// 對於 註冊的 轉發 客戶端 多久執行一次 活躍檢測 
			// 最小值是 Second
			ReadyCheck: Minute,

			// 撥號失敗後 等待下次重新撥號的 最大延遲
			MaxDialInterval: Second * 64,
		},
		Server: [
			// ssh 
			{
				// 額外配置 爲空的項使用 Tunnel.Option 的設置
				Option: {},
				// 暴露的 公開地址
				PublicAccepter: [
					{
						// 監聽地址
						Listen: ":8000",
						// x509 如果 CertFile KeyFile 同時有值 使用 tls 安全傳輸
						//CertFile: "test.pem",
						//KeyFile: "test.key",
					},
					{
						Listen: ":8080",
						//CertFile: "test.pem",
						//KeyFile: "test.key",
						// 使用 websocket 協議 默認 tcp
						Protocol: UseWebsocket,
						// websocket 路由接口 必須是全路徑 
						Route: [
							"/ssh",
							"/scp",
							"/sftp",
						],						
					},
				],
				// 暴露的 內部地址 供要反向映射的客戶端 註冊
				PrivateAccepter: [
					{
						// 監聽地址
						Listen: ":4000",
						// x509 如果 CertFile KeyFile 同時有值 使用 tls 安全傳輸
						// CertFile: "test.pem",
						// KeyFile: "test.key",
					},
					{
						Listen: ":8080",
						// CertFile: "test.pem",
						// KeyFile: "test.key",
						// 使用 websocket 協議
						Protocol: UseWebsocket,
						// websocket 路由接口 必須是全路徑 
						Route: [
							"/private/ssh",
							"/private/scp",
							"/private/sftp",
						],						
					},
				],
			},
		],
	},
	// 反向映射通道 註冊客戶
	Client: {
		// Server 全局默認配置
		Option: {
			//  向服務器註冊 轉發通道時 使用的 rsa 密鑰 以供身份驗證
			PrivateKey: "rsa.pri",
			// 空閒連接 超時 時間 通常 應該 設置比 Tunnel.Option.ReadyCheck 大
			// 當 空閒連接 一段 時間沒有收到 服務器 心跳則認爲斷線 斷開連接
			ReadyTimeout: Minute * 2,
			// 當通道建立後 多長時間沒有時間 任爲是 超時斷線
			Timeout: Hour,
			// 在 轉發通道內 單個方向的 數據時 創建 緩衝區大小
			// 通道是 全雙工的 故每個通道有 兩個 緩衝區 分別用於 讀寫
			// 最小值是 1024
			ForwardBufferSize: 1024 * 32,
			// 向服務器 建立多少個 空閒的連接 已備使用 通常應該小於服務器設置的 Tunnel.Option.Backlog
			Backlog: 2,
		},
		Server: [
			{
				// 額外配置 爲空的項使用 Client.Option 的設置
				Option: {},
				// 數據轉發的目標地址
				//Target: "127.0.0.1:22",
				// 服務器地址
				Source: "127.0.0.1:4000",
				// // 是否 使用 tls 安全傳輸
				// Safe: true,
				// // 使用 tls 時 是否 跳過證書 驗證
				// SkipVerify: true,
			},
			{
				// 數據轉發的目標地址
				Target: "127.0.0.1:22",
				// 服務器地址
				Source: "127.0.0.1:8080",
				// 協議使用 wbesokcet
				Protocol: UseWebsocket,
				// websocket 請求的路徑
				Route: "/private/ssh",
				// // 是否 使用 tls 安全傳輸
				// Safe: true,
				// //使用 tls 時 是否 跳過證書 驗證
				// SkipVerify: true,
			},
		],
	},
	// 數據轉換通道
	// 爲 數據源 和 數據目標 建議一個 數據轉換的 通道
	Forward: {
		// Server 全局默認配置
		Option: {
			// 建立好的 通道內 多長時間沒有數據 則自動斷開連接
			// 最小值是 Second
			Timeout : Hour,
			// 在 轉發通道內 單個方向的 數據時 創建 緩衝區大小
			// 通道是 全雙工的 故每個通道有 兩個 緩衝區 分別用於 讀寫
			// 最小值是 1024
			ForwardBufferSize: 1024 * 32,
			// 創建多少個協程 執行 撥號
			// 最小值是 1
			Backlog: 5,

			// 向數據目標 撥號的 超時時間
			// 最小值是 Second
			DialTimeout: Second * 20,

			// 撥號失敗後 等待下次重新撥號的 最大延遲
			MaxDialInterval: Second * 64,
		},
		Server: [
			// ssh 
			{
				// 額外配置 爲空的項使用 Transform.Option 的設置
				Option: {},
				// 數據來源
				Source: [
					{
						// 監聽地址
						Listen: ":18000",
						// x509 如果 CertFile KeyFile 同時有值 使用 tls 安全傳輸
						//CertFile: "test.pem",
						//KeyFile: "test.key",
					},
					{
						Listen: ":8080",
						//CertFile: "test.pem",
						//KeyFile: "test.key",
						// 使用 websocket 協議 默認 tcp
						Protocol: UseWebsocket,
						// websocket 路由接口 必須是全路徑 
						Route: [
							"/forward/ssh",
							"/forward/scp",
							"/forward/sftp",
						],						
					},
				],
				// 數據目標
				Target: {
					// 目標地址
					// Target: ":18000",
					Target: "127.0.0.1:8080",
					// 使用 websocket 協議
					Protocol: UseWebsocket,
					// websocket 路由接口 必須是全路徑 
					Route: "/ssh",
					// // 是否 使用 tls 安全傳輸
					// Safe: true,
					// //使用 tls 時 是否 跳過證書 驗證
					// SkipVerify: true,
				},
			},
		],
	},
	Logger:{
		// zap http
		//HTTP:"localhost:20000",
		// log name
		//Filename:"logs/hole.log",
		// MB
		MaxSize:    100, 
		// number of files
		MaxBackups: 3,
		// day
		MaxAge:     28,
		// level : debug info warn error dpanic panic fatal
		Level :"debug",
		// 是否要 輸出 代碼位置
		Caller:true,
	},
}