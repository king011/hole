package forward

import (
	"path/filepath"
	"strings"

	"gitlab.com/king011/hole/utils"
)

// Source 數據源
type Source struct {
	// 監聽地址
	Listen string
	// x509 如果 CertFile KeyFile 同時有值 使用 tls 安全傳輸
	CertFile string
	KeyFile  string
	// 使用的協議 默認使用 tcp
	Protocol string
	// 使用 websocket 時的 路由接口 必須是全路徑
	Route []string
}

// Format 標準化配置
func (s *Source) Format(basePath string) (e error) {
	s.Listen = strings.TrimSpace(s.Listen)
	s.CertFile = strings.TrimSpace(s.CertFile)
	s.KeyFile = strings.TrimSpace(s.KeyFile)
	if s.CertFile != "" && s.KeyFile != "" {
		if filepath.IsAbs(s.CertFile) {
			s.CertFile = filepath.Clean(s.CertFile)
		} else {
			s.CertFile = utils.Abs(basePath, s.CertFile)
		}
		if filepath.IsAbs(s.KeyFile) {
			s.KeyFile = filepath.Clean(s.KeyFile)
		} else {
			s.KeyFile = utils.Abs(basePath, s.KeyFile)
		}
	}
	s.Protocol = strings.ToLower(strings.TrimSpace(s.Protocol))
	if s.Protocol == "" {
		s.Protocol = "tcp"
	}
	count := len(s.Route)
	for i := 0; i < count; i++ {
		s.Route[i] = strings.TrimSpace(s.Route[i])
	}
	return
}
