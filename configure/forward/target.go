package forward

import "strings"

// Target 數據目標
type Target struct {
	// 目標地址
	// Target: ":18000",
	Target string
	// 使用 websocket 協議
	Protocol string
	// websocket 路由接口 必須是全路徑
	Route string
	// 是否 使用 tls 安全傳輸
	Safe bool
	//使用 tls 時 是否 跳過證書 驗證
	SkipVerify bool
}

// Format 標準化配置
func (t *Target) Format(basePath string) (e error) {
	t.Target = strings.TrimSpace(t.Target)
	t.Protocol = strings.ToLower(strings.TrimSpace(t.Protocol))
	if t.Protocol == "" {
		t.Protocol = "tcp"
	}
	t.Route = strings.TrimSpace(t.Route)
	return
}
