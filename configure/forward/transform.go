package forward

// Forward 數據轉換通道
//
// 爲 數據源 和 數據目標 建議一個 數據轉換的 通道
type Forward struct {
	Option Option
	Server []Server
}

// Format 標準化配置
func (t *Forward) Format(basePath string) (e error) {
	e = t.Option.Format(basePath, nil)
	if e != nil {
		return
	}
	count := len(t.Server)
	for i := 0; i < count; i++ {
		e = t.Server[i].Format(basePath, &t.Option)
		if e != nil {
			return
		}
	}
	return
}
