package forward

// Server 服務器配置
type Server struct {
	Option Option
	Source []Source
	Target Target
}

// Format 標準化配置
func (s *Server) Format(basePath string, option *Option) (e error) {
	e = s.Option.Format(basePath, option)
	if e != nil {
		return
	}
	e = s.Target.Format(basePath)
	if e != nil {
		return
	}
	count := len(s.Source)
	for i := 0; i < count; i++ {
		e = s.Source[i].Format(basePath)
		if e != nil {
			return
		}
	}
	return
}
