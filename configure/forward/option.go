package forward

import (
	"time"
)

// Option 配置
type Option struct {
	// 建立好的 通道內 多長時間沒有數據 則自動斷開連接
	// 最小值是 Second
	Timeout time.Duration
	// 在 轉發通道內 單個方向的 數據時 創建 緩衝區大小
	// 通道是 全雙工的 故每個通道有 兩個 緩衝區 分別用於 讀寫
	// 最小值是 1024
	ForwardBufferSize int

	// 創建多少個協程 執行 撥號
	// 最小值是 1
	Backlog int

	// 向數據目標 撥號的 超時時間
	// 最小值是 Second
	DialTimeout time.Duration

	// 撥號失敗後 等待下次重新撥號的 最大延遲
	MaxDialInterval time.Duration
}

// Format 標準化配置
func (o *Option) Format(basePath string, opt *Option) (e error) {
	o.Timeout *= time.Millisecond
	if o.Timeout < time.Second {
		if opt == nil {
			o.Timeout = time.Hour
		} else {
			o.Timeout = opt.Timeout
		}
	}

	if o.ForwardBufferSize < 1024 {
		if opt == nil {
			o.ForwardBufferSize = 32 * 1024
		} else {
			o.ForwardBufferSize = opt.ForwardBufferSize
		}
	}
	if o.Backlog < 1 {
		if opt == nil {
			o.Backlog = 5
		} else {
			o.Backlog = opt.Backlog
		}
	}

	o.DialTimeout *= time.Millisecond
	if o.DialTimeout < time.Second {
		if opt == nil {
			o.DialTimeout = time.Second * 20
		} else {
			o.DialTimeout = opt.DialTimeout
		}
	}

	o.MaxDialInterval *= time.Millisecond
	if o.MaxDialInterval < time.Second {
		if opt == nil {
			o.MaxDialInterval = time.Second * 64
		} else {
			o.MaxDialInterval = opt.MaxDialInterval
		}
	}
	return
}
