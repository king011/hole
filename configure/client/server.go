package client

import "strings"

// Server 服務配置
type Server struct {
	Option Option
	// 數據轉發的目標地址
	Target string
	// 服務器地址
	Source string
	// 協議
	Protocol string
	// websocket 請求的路徑
	Route string
	// 是否 使用 tls 安全傳輸
	Safe bool
	// 使用 tls 時 是否 跳過證書 驗證
	SkipVerify bool
}

// Format 標準化配置
func (s *Server) Format(basePath string, opt *Option) (e error) {
	e = s.Option.Format(basePath, opt)
	if e != nil {
		return
	}
	s.Target = strings.TrimSpace(s.Target)
	s.Source = strings.TrimSpace(s.Source)
	s.Protocol = strings.ToLower(strings.TrimSpace(s.Protocol))
	if s.Protocol == "" {
		s.Protocol = "tcp"
	}
	s.Route = strings.TrimSpace(s.Route)
	return
}
