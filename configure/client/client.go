package client

// Client 客戶端 配置
type Client struct {
	Option Option
	Server []Server
}

// Format 標準化配置
func (c *Client) Format(basePath string) (e error) {
	e = c.Option.Format(basePath, nil)
	if e != nil {
		return
	}
	count := len(c.Server)
	for i := 0; i < count; i++ {
		e = c.Server[i].Format(basePath, &c.Option)
		if e != nil {
			return
		}
	}
	return
}
