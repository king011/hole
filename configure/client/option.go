package client

import (
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/king011/hole/utils"
)

// Option 客戶端 配置
type Option struct {
	//  向服務器註冊 轉發通道時 使用的 rsa 密鑰 以供身份驗證
	PrivateKey string
	// 空閒連接 超時 時間 通常 應該 設置比 Tunnel.Option.ReadyCheck 大
	// 當 空閒連接 一段 時間沒有收到 服務器 心跳則認爲斷線 斷開連接
	ReadyTimeout time.Duration
	// 當通道建立後 多長時間沒有時間 任爲是 超時斷線
	Timeout time.Duration
	// 在 轉發通道內 單個方向的 數據時 創建 緩衝區大小
	// 通道是 全雙工的 故每個通道有 兩個 緩衝區 分別用於 讀寫
	// 最小值是 1024
	ForwardBufferSize int
	// 向服務器 建立多少個 空閒的連接 已備使用 通常應該小於服務器設置的 Tunnel.Option.Backlog
	Backlog int
}

// Format 標準化配置
func (o *Option) Format(basePath string, opt *Option) (e error) {
	o.PrivateKey = strings.TrimSpace(o.PrivateKey)
	if o.PrivateKey == "" {
		if opt == nil {
			o.PrivateKey = utils.Abs(basePath, "rsa.pri")
		} else {
			o.PrivateKey = opt.PrivateKey
		}
	} else {
		if filepath.IsAbs(o.PrivateKey) {
			o.PrivateKey = filepath.Clean(o.PrivateKey)
		} else {
			o.PrivateKey = utils.Abs(basePath, o.PrivateKey)
		}
	}

	o.ReadyTimeout *= time.Millisecond
	if o.ReadyTimeout < time.Second {
		if opt == nil {
			o.ReadyTimeout = time.Minute * 2
		} else {
			o.ReadyTimeout = opt.ReadyTimeout
		}
	}
	o.Timeout *= time.Millisecond
	if o.Timeout < time.Second {
		if opt == nil {
			o.Timeout = time.Hour
		} else {
			o.Timeout = opt.Timeout
		}
	}
	if o.ForwardBufferSize < 1024 {
		if opt == nil {
			o.ForwardBufferSize = 1024 * 32
		} else {
			o.ForwardBufferSize = opt.ForwardBufferSize
		}
	}

	if o.Backlog < 1 {
		if opt == nil {
			o.Backlog = 2
		} else {
			o.Backlog = opt.Backlog
		}
	}

	return
}
