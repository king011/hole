package tunnel

// Tunnel 反向映射通道 配置
type Tunnel struct {
	Option Option
	Server []Server
}

// Format 標準化配置
func (t *Tunnel) Format(basePath string) (e error) {
	e = t.Option.Format(basePath, nil)
	if e != nil {
		return
	}
	count := len(t.Server)
	for i := 0; i < count; i++ {
		e = t.Server[i].Format(basePath, &t.Option)
		if e != nil {
			return
		}
	}
	return
}
