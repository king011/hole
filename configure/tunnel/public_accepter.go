package tunnel

import (
	"path/filepath"
	"strings"

	"gitlab.com/king011/hole/utils"
)

// Accepter .
type Accepter struct {
	// 監聽地址
	Listen string
	// x509 如果 CertFile KeyFile 同時有值 使用 tls 安全傳輸
	CertFile string
	KeyFile  string
	// 使用的協議 默認使用 tcp
	Protocol string
	// 使用 websocket 時的 路由接口 必須是全路徑
	Route []string
}

// Format 標準化配置
func (a *Accepter) Format(basePath string) (e error) {
	a.Listen = strings.TrimSpace(a.Listen)
	a.CertFile = strings.TrimSpace(a.CertFile)
	a.KeyFile = strings.TrimSpace(a.KeyFile)
	if a.CertFile != "" && a.KeyFile != "" {
		if filepath.IsAbs(a.CertFile) {
			a.CertFile = filepath.Clean(a.CertFile)
		} else {
			a.CertFile = utils.Abs(basePath, a.CertFile)
		}
		if filepath.IsAbs(a.KeyFile) {
			a.KeyFile = filepath.Clean(a.KeyFile)
		} else {
			a.KeyFile = utils.Abs(basePath, a.KeyFile)
		}
	}
	a.Protocol = strings.ToLower(strings.TrimSpace(a.Protocol))
	if a.Protocol == "" {
		a.Protocol = "tcp"
	}
	count := len(a.Route)
	for i := 0; i < count; i++ {
		a.Route[i] = strings.TrimSpace(a.Route[i])
	}
	return
}
