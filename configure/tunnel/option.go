package tunnel

import (
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/king011/hole/utils"
)

// Option 配置
type Option struct {
	//  客戶註冊時 安全驗證使用的 公鑰
	// 必須是有效的 rsa public key
	PublicKey string
	// 建立好的 通道內 多長時間沒有數據 則自動斷開連接
	// 最小值是 Second
	Timeout time.Duration
	// 在 轉發通道內 單個方向的 數據時 創建 緩衝區大小
	// 通道是 全雙工的 故每個通道有 兩個 緩衝區 分別用於 讀寫
	// 最小值是 1024
	ForwardBufferSize int

	// 如果 轉發客戶端端 註冊了一個 PrivateAccepter 此時沒有 數據源連接 PublicAccepter 將積壓一個 空閒的 通道
	// Backlog 設置允許的最大積壓數
	// 達到 Backlog 上限 PrivateAccepter 將 停止 接收 新的 註冊
	// 最小值是 1
	Backlog int

	// PrivateAccepter 向註冊的 轉發客戶 發送 撥號請求的 超時時間
	// 最小值是 Second
	DialTimeout time.Duration
	// 等待響應的 超時時間
	// 最小值是 Second
	MessageTimeout time.Duration
	// 同時允許的 最大 撥號數量
	// 最小值是 1
	DialerBacklog int
	// 對於 註冊的 轉發 客戶端 多久執行一次 活躍檢測
	// 最小值是 Second
	ReadyCheck time.Duration

	// 撥號失敗後 等待下次重新撥號的 最大延遲
	MaxDialInterval time.Duration
}

// Format 標準化配置
func (o *Option) Format(basePath string, opt *Option) (e error) {
	o.PublicKey = strings.TrimSpace(o.PublicKey)
	if o.PublicKey == "" {
		if opt == nil {
			o.PublicKey = utils.Abs(basePath, "rsa.pub")
		} else {
			o.PublicKey = opt.PublicKey
		}
	} else {
		if filepath.IsAbs(o.PublicKey) {
			o.PublicKey = filepath.Clean(o.PublicKey)
		} else {
			o.PublicKey = utils.Abs(basePath, o.PublicKey)
		}
	}

	o.Timeout *= time.Millisecond
	if o.Timeout < time.Second {
		if opt == nil {
			o.Timeout = time.Hour
		} else {
			o.Timeout = opt.Timeout
		}
	}

	if o.ForwardBufferSize < 1024 {
		if opt == nil {
			o.ForwardBufferSize = 32 * 1024
		} else {
			o.ForwardBufferSize = opt.ForwardBufferSize
		}
	}
	if o.Backlog < 1 {
		if opt == nil {
			o.Backlog = 5
		} else {
			o.Backlog = opt.Backlog
		}
	}

	o.DialTimeout *= time.Millisecond
	if o.DialTimeout < time.Second {
		if opt == nil {
			o.DialTimeout = time.Second * 20
		} else {
			o.DialTimeout = opt.DialTimeout
		}
	}
	o.MessageTimeout *= time.Millisecond
	if o.MessageTimeout < time.Second {
		if opt == nil {
			o.MessageTimeout = time.Second * 20
		} else {
			o.MessageTimeout = opt.MessageTimeout
		}
	}

	if o.DialerBacklog < 1 {
		if opt == nil {
			o.DialerBacklog = 5
		} else {
			o.DialerBacklog = opt.DialerBacklog
		}
	}

	o.ReadyCheck *= time.Millisecond
	if o.ReadyCheck < time.Second {
		if opt == nil {
			o.ReadyCheck = time.Minute
		} else {
			o.ReadyCheck = opt.ReadyCheck
		}
	}

	o.MaxDialInterval *= time.Millisecond
	if o.MaxDialInterval < time.Second {
		if opt == nil {
			o.MaxDialInterval = time.Second * 64
		} else {
			o.MaxDialInterval = opt.MaxDialInterval
		}
	}
	return
}
