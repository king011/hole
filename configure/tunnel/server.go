package tunnel

// Server 服務器配置
type Server struct {
	Option          Option
	PublicAccepter  []Accepter
	PrivateAccepter []Accepter
}

// Format 標準化配置
func (s *Server) Format(basePath string, option *Option) (e error) {
	e = s.Option.Format(basePath, option)
	if e != nil {
		return
	}
	count := len(s.PublicAccepter)
	for i := 0; i < count; i++ {
		e = s.PublicAccepter[i].Format(basePath)
		if e != nil {
			return
		}
	}
	count = len(s.PrivateAccepter)
	for i := 0; i < count; i++ {
		e = s.PrivateAccepter[i].Format(basePath)
		if e != nil {
			return
		}
	}
	return
}
