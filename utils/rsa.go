package utils

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"os"
)

// SavePublicKey 保存 rsa public key 到 檔案
func SavePublicKey(filename string, publicKey *rsa.PublicKey) (e error) {
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		return
	}
	defer f.Close()
	block := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: x509.MarshalPKCS1PublicKey(publicKey),
	}
	e = pem.Encode(f, block)
	return
}

// SavePrivateKey 保存 rsa private key 到 檔案
func SavePrivateKey(filename string, privateKey *rsa.PrivateKey) (e error) {
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		return
	}
	defer f.Close()
	block := &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
	}
	e = pem.Encode(f, block)
	return
}

// LoadPublicKey 從 檔案加載 rsa public key
func LoadPublicKey(filename string) (publicKey *rsa.PublicKey, e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	block, _ := pem.Decode(b)
	publicKey, e = x509.ParsePKCS1PublicKey(block.Bytes)
	return
}

// LoadPrivateKey 從 檔案加載 rsa private key
func LoadPrivateKey(filename string) (privateKey *rsa.PrivateKey, e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	block, _ := pem.Decode(b)
	privateKey, e = x509.ParsePKCS1PrivateKey(block.Bytes)
	return
}
